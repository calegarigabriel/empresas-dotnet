﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Empresas.Domain.Identity;
using Microsoft.AspNetCore.Http;

namespace Empresas.Infra.CrossCutting.Identity
{
    public class AspNetUser : IUser
    {
        private readonly IHttpContextAccessor accessor;

        public AspNetUser(IHttpContextAccessor accessor)
        {
            this.accessor = accessor;
        }

        public string Name => GetName();
        private string GetName()
        {
            return accessor.HttpContext.User.Identity.Name ??
                   accessor.HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value;
        }

        public bool IsAuthenticated()
        {
            return accessor.HttpContext.User.Identity.IsAuthenticated;
        }

        public IEnumerable<Claim> GetClaimsIdentity()
        {
            return accessor.HttpContext.User.Claims;
        }
    }
}
