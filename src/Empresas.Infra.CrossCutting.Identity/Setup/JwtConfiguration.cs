﻿namespace Empresas.Infra.CrossCutting.Identity.Setup
{
    public class JwtConfiguration
    {
        public string Secret { get; set; }
        public int Expiration { get; set; }
        public string Issuer { get; set; }
        public string ValidAt { get; set; }
    }
}