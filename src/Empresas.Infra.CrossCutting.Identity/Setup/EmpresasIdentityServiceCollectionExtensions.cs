﻿using System;
using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;

namespace Empresas.Infra.CrossCutting.Identity.Setup
{
    public static class EmpresasIdentityServiceCollectionExtensions
    {
        public static void AddIdentitySetup(this IServiceCollection services, IConfiguration configuration)
        {
            services = services ?? throw new ArgumentNullException(nameof(services));
            configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

            var empresasIdentityConfiguration = new EmpresasIdentityConfiguration();
            configuration.Bind("EmpresasIdentityConfiguration", empresasIdentityConfiguration);

            var jwtConfiguration = new JwtConfiguration();
            configuration.Bind("JwtConfiguration", jwtConfiguration);

            services.AddSingleton(jwtConfiguration);

            services.AddDbContext<IdentityDbContext>(options =>
                options.UseSqlServer(
                    empresasIdentityConfiguration.SqlConnectionString,
                    builder => builder.MigrationsAssembly(Assembly.GetExecutingAssembly().FullName)));

            services.AddDefaultIdentity<IdentityUser>(
                    options =>
                    {
                        options.Password.RequireNonAlphanumeric = false;
                        options.Password.RequireLowercase = false;
                        options.Password.RequireUppercase = false;
                    })
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<IdentityDbContext>()
                .AddDefaultTokenProviders();

            var key = Encoding.ASCII.GetBytes(jwtConfiguration.Secret);
            
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = true;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidAudience = jwtConfiguration.ValidAt,
                    ValidIssuer = jwtConfiguration.Issuer
                };
            });

            services.AddAuthorization(options =>
            {
                var defaultAuthorizationPolicyBuilder = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme);
                defaultAuthorizationPolicyBuilder = defaultAuthorizationPolicyBuilder.RequireAuthenticatedUser();
                options.DefaultPolicy = defaultAuthorizationPolicyBuilder.Build();
            });
        }

        public static void AddAuthSetup(this IServiceCollection services, IConfiguration configuration)
        {
            if (services == null) throw new ArgumentNullException(nameof(services));

            services.AddAuthorization();
        }
    }
}
