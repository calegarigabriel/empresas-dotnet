﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Empresas.Infra.CrossCutting.Identity
{
    public static class IdentityContextSeed
    {
        public static IHost SeedIdentityData(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                using (var dataContext = scope.ServiceProvider.GetRequiredService<IdentityDbContext>())
                {
                    try
                    {
                        var userManager = scope.ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();
                        RunSeed(dataContext, userManager);
                    }
                    catch (Exception ex)
                    {
                        var logger = scope.ServiceProvider.GetRequiredService<ILogger<IdentityDbContext>>();
                        logger.LogError(ex, "An error occurred seeding the DB.");

                        throw;
                    }
                }
            }

            return host;
        }

        private static void RunSeed(IdentityDbContext context, UserManager<IdentityUser> userManager)
        {
            if (context.Users.Any())
                return;

            var user = new IdentityUser
            {
                UserName = "testeapple@ioasys.com.br",
                Email = "testeapple@ioasys.com.br",
            };
            
            userManager.CreateAsync(user, "12341234")
                .GetAwaiter()
                .GetResult();

            context.SaveChanges();
        }
    }
}
