﻿namespace Empresas.Domain.Core.DomainObjects
{
    /// <summary>
    /// Marker interface for aggregate root
    /// </summary>
    public interface IAggregateRoot
    {
    }
}
