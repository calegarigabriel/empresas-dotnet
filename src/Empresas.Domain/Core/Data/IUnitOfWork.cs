﻿using System.Threading.Tasks;

namespace Empresas.Domain.Core.Data
{
    public interface IUnitOfWork
    {
        Task<bool> Commit();
    }
}
