﻿using Empresas.Domain.Core.DomainObjects;
using System;

namespace Empresas.Domain.Core.Data
{
    public interface IRepository<T> : IDisposable where T : IAggregateRoot
    {
        IUnitOfWork UnitOfWork { get; }
    }
}
