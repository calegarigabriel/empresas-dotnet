﻿using Empresas.Domain.Core.DomainObjects;
using System.Collections.Generic;

namespace Empresas.Domain.Models
{
    /// <summary>
    /// Optei por considerar uma entidade em vez de um enumerador,
    /// pensando que o número de tipos pode aumentar
    /// </summary>
    public class EnterpriseType : Entity
    {
        public int Code { get; private set; }
        public string Name { get; private set; }

        // EF Relation
        public ICollection<Enterprise> Enterprises { get; set; }

        private EnterpriseType() { }

        public EnterpriseType(int code, string name)
        {
            Code = code;
            Name = name;
        }
    }
}
