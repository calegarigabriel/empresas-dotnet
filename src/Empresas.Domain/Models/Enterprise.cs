﻿using Empresas.Domain.Core.DomainObjects;
using System;

namespace Empresas.Domain.Models
{
    public class Enterprise : Entity, IAggregateRoot
    {
        public int Code { get; private set; }
        public string Name { get; private set; }

        public EnterpriseType Type { get; private set; }
        public Guid TypeId { get; private set; }

        private Enterprise() { }

        public Enterprise(int code, string name, Guid typeId)
        {
            Code = code;
            Name = name;
            TypeId = typeId;
        }
    }
}
