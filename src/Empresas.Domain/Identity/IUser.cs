﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Empresas.Domain.Identity
{
    public interface IUser
    {
        string Name { get; }
        bool IsAuthenticated();
        IEnumerable<Claim> GetClaimsIdentity();
    }
}
