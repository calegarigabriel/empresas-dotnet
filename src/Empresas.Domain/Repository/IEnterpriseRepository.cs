﻿using Empresas.Domain.Core.Data;
using Empresas.Domain.Models;
using Empresas.Domain.Repository.Filters;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Empresas.Domain.Repository
{
    public interface IEnterpriseRepository : IRepository<Enterprise>
    {
        Task<IEnumerable<Enterprise>> GetAll(EnterpriseFilter filter);

        Task<Enterprise> GetByCode(int code);

        Task<int> Count();
    }
}
