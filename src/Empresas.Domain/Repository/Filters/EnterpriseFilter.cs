﻿namespace Empresas.Domain.Repository.Filters
{
    public class EnterpriseFilter : BaseFilter
    {
        public string Name { get; set; }

        public int? Type { get; set; }
    }
}
