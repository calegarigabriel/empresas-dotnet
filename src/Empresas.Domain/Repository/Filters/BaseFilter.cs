﻿namespace Empresas.Domain.Repository.Filters
{
    public abstract class BaseFilter
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
