﻿using System;
using Empresas.Domain.Core.Data;
using Empresas.Domain.Repository;
using Empresas.Infra.Data.Repository;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Empresas.Infra.Data.Setup
{
    public static class EmpresasDataServiceCollectionExtensions
    {
        public static void AddEmpresasDataSetup(this IServiceCollection services, IConfiguration configuration)
        {
            services = services ?? throw new ArgumentNullException(nameof(services));
            configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));

            var empresasDataConfiguration = new EmpresasDataConfiguration();
            configuration.Bind("EmpresasDataConfiguration", empresasDataConfiguration);

            services.AddDbContext<EmpresasContext>(options =>
                options.UseSqlServer(empresasDataConfiguration.SqlConnectionString));

            services.AddScoped<IEnterpriseRepository, EnterpriseRepository>();
            services.AddScoped<IUnitOfWork, EmpresasContext>();
        }
    }
}
