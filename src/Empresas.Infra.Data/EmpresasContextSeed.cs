﻿using System;
using System.Collections.Generic;
using System.Linq;
using Empresas.Domain.Models;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Empresas.Infra.Data
{
    public static class EmpresasContextSeed
    {
        public static IHost SeedEmpresasData(this IHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                using (var dataContext = scope.ServiceProvider.GetRequiredService<EmpresasContext>())
                {
                    try
                    {
                        RunSeed(dataContext);
                    }
                    catch (Exception ex)
                    {
                        var logger = scope.ServiceProvider.GetRequiredService<ILogger<EmpresasContext>>();
                        logger.LogError(ex, "An error occurred seeding the DB.");

                        throw;
                    }
                }
            }

            return host;
        }

        private static void RunSeed(EmpresasContext context)
        {
            if (context.Enterprises.Any())
                return;

            var types = EnterpriseTypes.ToArray();
            context.EnterpriseTypes.AddRange(types);

            var enterprise1 = new Enterprise(1, "AT&T", types.Single(t => t.Code == 22).Id);
            var enterprise2 = new Enterprise(2, "Ioasys", types.Single(t => t.Code == 21).Id);
            var enterprise3 = new Enterprise(3, "Anima", types.Single(t => t.Code == 6).Id);
            var enterprise4 = new Enterprise(4, "TecAgro", types.Single(t => t.Code == 1).Id);
            var enterprise5 = new Enterprise(5, "BellaAgro", types.Single(t => t.Code == 1).Id);

            context.Enterprises.AddRange(
                enterprise1, 
                enterprise2,
                enterprise3,
                enterprise4,
                enterprise5);

            context.SaveChanges();
        }

        private static IEnumerable<EnterpriseType> EnterpriseTypes
        {
            get
            {
                yield return new EnterpriseType(1, "Agro");
                yield return new EnterpriseType(2, "Aviation");
                yield return new EnterpriseType(3, "Biotech");
                yield return new EnterpriseType(4, "Eco");
                yield return new EnterpriseType(5, "Ecommerce");
                yield return new EnterpriseType(6, "Education");
                yield return new EnterpriseType(7, "Fashion");
                yield return new EnterpriseType(8, "Fintech");
                yield return new EnterpriseType(9, "Food");
                yield return new EnterpriseType(10, "Games");
                yield return new EnterpriseType(11, "Health");
                yield return new EnterpriseType(12, "IOT");
                yield return new EnterpriseType(13, "Logistics");
                yield return new EnterpriseType(14, "Media");
                yield return new EnterpriseType(15, "Mining");
                yield return new EnterpriseType(16, "Products");
                yield return new EnterpriseType(17, "Real Estate");
                yield return new EnterpriseType(18, "Service");
                yield return new EnterpriseType(19, "Smart City");
                yield return new EnterpriseType(20, "Social");
                yield return new EnterpriseType(21, "Software");
                yield return new EnterpriseType(22, "Technology");
                yield return new EnterpriseType(23, "Tourism");
                yield return new EnterpriseType(24, "Transport");
            }
        }
    }
}
