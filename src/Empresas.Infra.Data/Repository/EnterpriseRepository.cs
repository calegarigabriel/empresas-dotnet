﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Empresas.Domain.Core.Data;
using Empresas.Domain.Models;
using Empresas.Domain.Repository;
using Empresas.Domain.Repository.Filters;
using Microsoft.EntityFrameworkCore;

namespace Empresas.Infra.Data.Repository
{
    public class EnterpriseRepository : IEnterpriseRepository
    {
        private readonly EmpresasContext empresasContext;
        public IUnitOfWork UnitOfWork => empresasContext;

        public EnterpriseRepository(EmpresasContext empresasContext)
        {
            this.empresasContext = empresasContext ?? throw new ArgumentNullException(nameof(empresasContext));
        }

        public async Task<IEnumerable<Enterprise>> GetAll(EnterpriseFilter filter)
        {
            var query = empresasContext.Enterprises
                .AsNoTracking()
                .AsQueryable();

            if (!string.IsNullOrEmpty(filter.Name))
            {
                query = query.Where(e => e.Name == filter.Name);
            }

            if (filter.Type.HasValue)
            {
                query = query.Where(e => e.Type.Code == filter.Type.Value);
            }

            return await query
                .Include(e => e.Type)
                .Skip((filter.PageIndex - 1) * filter.PageSize)
                .Take(filter.PageSize)
                .ToListAsync();
        }

        public Task<Enterprise> GetByCode(int code)
        {
            return empresasContext.Enterprises
                    .AsNoTracking()
                    .Where(e => e.Code == code)
                    .Include(e => e.Type)
                    .FirstOrDefaultAsync();
        }

        public Task<int> Count()
        {
            return empresasContext.Enterprises.CountAsync();
        }

        public void Dispose()
        {
            empresasContext?.Dispose();
        }
    }
}
