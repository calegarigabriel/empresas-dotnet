﻿using System.Linq;
using System.Threading.Tasks;
using Empresas.Domain.Core.Data;
using Empresas.Domain.Models;
using Microsoft.EntityFrameworkCore;

namespace Empresas.Infra.Data
{
    public class EmpresasContext : DbContext, IUnitOfWork
    {
        public EmpresasContext(DbContextOptions<EmpresasContext> options) : base(options)
        {
        }

        public DbSet<Enterprise> Enterprises { get; set; }
        public DbSet<EnterpriseType> EnterpriseTypes { get; set; }

        public async Task<bool> Commit()
        {
            return await base.SaveChangesAsync() > 0;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var property in modelBuilder.Model.GetEntityTypes().SelectMany(
                e => e.GetProperties().Where(p => p.ClrType == typeof(string))))
                property.SetColumnType("varchar(64)");

            modelBuilder.ApplyConfigurationsFromAssembly(typeof(EmpresasContext).Assembly);
        }
    }
}
