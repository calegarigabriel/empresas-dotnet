﻿using Empresas.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Empresas.Infra.Data.Mappings
{
    internal class EnterpriseMapping : IEntityTypeConfiguration<Enterprise>
    {
        public void Configure(EntityTypeBuilder<Enterprise> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasIndex(e => e.Code)
                .IsUnique();

            builder.Property(e => e.Name)
                .IsRequired();

            builder.ToTable("Enterprises");
        }
    }
}
