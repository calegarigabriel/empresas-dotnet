﻿using Empresas.Domain.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Empresas.Infra.Data.Mappings
{
    internal class EnterpriseTypeMapping : IEntityTypeConfiguration<EnterpriseType>
    {
        public void Configure(EntityTypeBuilder<EnterpriseType> builder)
        {
            builder.HasKey(e => e.Id);
            builder.HasIndex(e => e.Code)
                .IsUnique();

            builder.Property(e => e.Name)
                .IsRequired();

            // 1 : N => EnterpriseType : Enterprises
            builder.HasMany(e => e.Enterprises)
                .WithOne(e => e.Type)
                .HasForeignKey(e => e.TypeId);

            builder.ToTable("EnterpriseTypes");
        }
    }
}
