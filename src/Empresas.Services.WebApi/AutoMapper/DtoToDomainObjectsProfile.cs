﻿using AutoMapper;
using Empresas.Domain.Repository.Filters;
using Empresas.Services.WebApi.Dtos;

namespace Empresas.Services.WebApi.AutoMapper
{
    public class DtoToDomainObjectsProfile : Profile
    {
        public DtoToDomainObjectsProfile()
        {
            CreateMap<EnterprisesGet, EnterpriseFilter>()
                .ForMember(enterprisesGet => enterprisesGet.Type, e => e.MapFrom(f => f.EnterpriseType));
        }
    }
}
