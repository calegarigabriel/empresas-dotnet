﻿using AutoMapper;
using Empresas.Domain.Models;
using Empresas.Services.WebApi.Dtos;

namespace Empresas.Services.WebApi.AutoMapper
{
    public class DomainObjectsToDtoProfile : Profile
    {
        public DomainObjectsToDtoProfile()
        {
            CreateMap<EnterpriseType, EnterpriseTypeDto>();
            CreateMap<Enterprise, EnterpriseDto>();
        }
    }
}
