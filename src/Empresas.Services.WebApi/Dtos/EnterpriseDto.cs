﻿namespace Empresas.Services.WebApi.Dtos
{
    public class EnterpriseDto
    {
        public int Code { get; set; }

        public string Name { get; set; }

        public EnterpriseTypeDto Type { get; set; }
    }
}
