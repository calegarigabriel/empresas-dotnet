﻿using Microsoft.AspNetCore.Mvc;

namespace Empresas.Services.WebApi.Dtos
{
    public class EnterprisesGet
    {
        public string Name { get; set; }

        [FromQuery(Name = "enterprise_types")]
        public int? EnterpriseType { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; }
    }
}
