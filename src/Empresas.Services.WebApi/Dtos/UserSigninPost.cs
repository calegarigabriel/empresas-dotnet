﻿using System.ComponentModel.DataAnnotations;

namespace Empresas.Services.WebApi.Dtos
{
    public class UserSigninPost
    {
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}