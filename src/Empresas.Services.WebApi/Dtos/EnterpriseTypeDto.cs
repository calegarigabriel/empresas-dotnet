﻿namespace Empresas.Services.WebApi.Dtos
{
    public class EnterpriseTypeDto
    {
        public int Code { get; set; }

        public string Name { get; set; }
    }
}
