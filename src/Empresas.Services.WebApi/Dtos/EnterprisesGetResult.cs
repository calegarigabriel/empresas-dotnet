﻿using System.Collections.Generic;

namespace Empresas.Services.WebApi.Dtos
{
    public class EnterprisesGetResult
    {
        public int PageIndex { get; }
        public int PageSize { get; }
        public int Total { get; }

        public IEnumerable<EnterpriseDto> Enterprises { get; set; }

        public EnterprisesGetResult(
            int pageIndex, 
            int pageSize, 
            int total, 
            IEnumerable<EnterpriseDto> enterprises)
        {
            PageIndex = pageIndex;
            PageSize = pageSize;
            Total = total;
            Enterprises = enterprises;
        }
    }
}
