﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Empresas.Infra.CrossCutting.Identity.Setup;
using Empresas.Services.WebApi.Dtos;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Empresas.Services.WebApi.Controllers
{
    [ApiController, ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly SignInManager<IdentityUser> signInManager;
        private readonly UserManager<IdentityUser> userManager;
        private readonly JwtConfiguration jwtConfiguration;

        public UsersController(
            SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            JwtConfiguration jwtConfiguration)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.jwtConfiguration = jwtConfiguration;
        }

        [Route("auth/sign_in")]
        [HttpPost]
        public async Task<IActionResult> Signin(UserSigninPost userSignin)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var result = await signInManager.PasswordSignInAsync(userSignin.Email, userSignin.Password, false, true);

            if (result.Succeeded)
            {
                var token = await GenerateJwt(userSignin.Email);
                return Ok(new { token });
            }

            return BadRequest(new
            {
                success = false
            });
        }

        private async Task<string> GenerateJwt(string email)
        {
            var user = await userManager.FindByEmailAsync(email);
            var claims = await userManager.GetClaimsAsync(user);

            claims.Add(new Claim(JwtRegisteredClaimNames.Sub, user.Id));
            claims.Add(new Claim(JwtRegisteredClaimNames.Email, user.Email));

            var identityClaims = new ClaimsIdentity();
            identityClaims.AddClaims(claims);

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(jwtConfiguration.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = identityClaims,
                Issuer = jwtConfiguration.Issuer,
                Audience = jwtConfiguration.ValidAt,
                Expires = DateTime.UtcNow.AddHours(jwtConfiguration.Expiration),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            return tokenHandler.WriteToken(tokenHandler.CreateToken(tokenDescriptor));
        }
    }
}