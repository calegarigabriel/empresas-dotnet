﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Empresas.Domain.Repository;
using Empresas.Domain.Repository.Filters;
using Empresas.Services.WebApi.Dtos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Empresas.Services.WebApi.Controllers
{
    [ApiController, ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [Authorize]
    public class EnterprisesController : ControllerBase
    {
        private readonly IEnterpriseRepository enterpriseRepository;
        private readonly IMapper mapper;

        public EnterprisesController(
            IEnterpriseRepository enterpriseRepository,
            IMapper mapper
            )
        {
            this.enterpriseRepository =
                enterpriseRepository ?? throw new ArgumentNullException(nameof(enterpriseRepository));
            
            this.mapper = 
                mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] EnterprisesGet enterprisesGet)
        {
            enterprisesGet.PageIndex = enterprisesGet.PageIndex > 0 ? enterprisesGet.PageIndex : 1;
            enterprisesGet.PageSize = enterprisesGet.PageSize <= 10 && enterprisesGet.PageSize > 0 ? enterprisesGet.PageSize : 10;

            var enterpriseFilter = mapper.Map<EnterpriseFilter>(enterprisesGet);
            var enterprises = await enterpriseRepository.GetAll(enterpriseFilter);
            var enterprisesDto = mapper.Map<IEnumerable<EnterpriseDto>>(enterprises);

            var total = await enterpriseRepository.Count();
            var result = new EnterprisesGetResult(
                enterprisesGet.PageIndex,
                enterprisesGet.PageSize,
                total,
                enterprisesDto);

            return Ok(result);
        }

        [HttpGet("{code}")]
        public async Task<IActionResult> Get(int code)
        {
            var enterprise = await enterpriseRepository.GetByCode(code);
            
            if (enterprise == null)
                return NotFound();

            var result = mapper.Map<EnterpriseDto>(enterprise);
            return Ok(result);
        }
    }
}